import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchCinemaBranch,
  fetchCinemaList,
} from "../../redux/actions/cinema.actions";
import "./style.scss";
import { Box, Button } from "@material-ui/core";
import { Fragment } from "react";

// function TabPanel(props) {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`vertical-tabpanel-${index}`}
//       aria-labelledby={`vertical-tab-${index}`}
//       {...other}
//     >
//       {value === index && <Button p={3}>{children}</Button>}
//     </div>
//   );
// }

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    height: 500,
    marginTop: 30,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

export default function ShowTimes() {
  const classes = useStyles();
  const [value, setvalue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setvalue(newValue);
  };
  const [valueChildren, setvalueChildren] = React.useState(1000);
  const handleChangeChildren = (event, newValueChildren) => {
    console.log(newValueChildren);
    setvalue(newValueChildren);
  };
  const cinemaList = useSelector((state) => {
    return state.cinema?.cinemaList;
  });
  const cinemaListBranch = useSelector((state) => {
    return state.cinema?.cinemaListBranch;
  });
  const dispatch = useDispatch();
  useEffect(function () {
    dispatch(fetchCinemaList());
    for (let item in cinemaListBranch) {
      dispatch(fetchCinemaBranch(item));
    }
  }, []);

  function imgLogo(props) {
    return (
      <img
        src={props.logo}
        alt={props.maHeThongRap}
        className="showTimes__logo--item"
      ></img>
    );
  }
  function renderLogo() {
    return cinemaList?.map((item, index) => {
      return (
        <Tab
          style={{ padding: 10 }}
          icon={imgLogo(item)}
          {...a11yProps(item.maHeThongRap)}
          key={item.maHeThongRap}
          className="showTimes__logo"
        />
      );
    });
  }
  function renderTab() {
    console.log(value, valueChildren);
    return cinemaList?.map((item, index) => {
      return (
        <div
          role="tabpanel"
          hidden={value + item.maHeThongRap !== index + item.maHeThongRap}
          id={`vertical-tabpanel-${item.maHeThongRap}`}
          aria-labelledby={`vertical-tab-${item.maHeThongRap}`}
          key={index}
        >
          {value + item.maHeThongRap === index + item.maHeThongRap && (
            <Tabs
              orientation="vertical"
              variant="scrollable"
              aria-label="Vertical tabs example"
              className={classes.tabs}
              valueChildren={valueChildren}
              onChange={handleChangeChildren}
            >
              {/* {renderCinemaBranch(item.maHeThongRap)} */}
            </Tabs>
          )}
        </div>
      );
    });
  }
  // bị lỗi
  // function renderCinemaBranch(maHeThongRap) {
  //   for (let item in cinemaListBranch) {
  //     console.log(cinemaListBranch[item]);
  //     if (item === maHeThongRap) {
  //       return cinemaListBranch[item]?.map((item) => {
  //         return (
  //           <Tab
  //             style={{ padding: 10 }}
  //             label={item.maCumRap}
  //             {...a11yProps(item.maCumRap)}
  //             key={item.maCumRap}
  //             className="showTimes__branch"
  //           />
  //         );
  //       });
  //     }
  //   }
  // }
  function TabPanel(maHeThongRap) {
    for (let item in cinemaListBranch) {
      if (item === maHeThongRap) {
        return cinemaListBranch[maHeThongRap].map((item, index) => {
          return (
            <div
              role="tabpanel"
              hidden={value + item.maCumRap !== index + item.maCumRap}
              id={`vertical-tabpanel-${item.maCumRap}`}
              aria-labelledby={`vertical-tab-${item.maCumRap}`}
            >
              {value + item.maCumRap === index + item.maCumRap && (
                <Box p={3}>
                  <Typography>{item.maCumRap}</Typography>
                </Box>
              )}
            </div>
          );
        });
      }
    }
  }

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        aria-label="Vertical tabs example"
        className={classes.tabs}
        value={value}
        onChange={handleChange}
      >
        {renderLogo()}
      </Tabs>
      {renderTab(value)}
      {/* <TabPanel value={value} index={0}>
        Item One
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
      <TabPanel value={value} index={4}>
        Item Five
      </TabPanel>
      <TabPanel value={value} index={5}>
        Item Six
      </TabPanel>
      <TabPanel value={value} index={6}>
        Item Seven
      </TabPanel> */}
    </div>
  );
}
