import {
  FETCHCINEMALISTSUCCESS,
  FETCHCINEMALISTFAILED,
  FETCHCINEMABRANCHSUCCESS,
  FETCHCINEMABRANCHFAILED,
} from "../constants/cinema.constant";
import Axios from "axios";

// FETCH CINEMA LIST
export const fetchCinemaList = () => {
  return (dispatch) => {
    Axios.get(
      "https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinHeThongRap"
    )
      .then((res) => {
        dispatch(fetchCinemaListSuccess(res.data));
        console.log(res.data, "cinemaList");
      })
      .catch((err) => {
        dispatch(fetchCinemaListFailed(err));
      });
  };
};

const fetchCinemaListSuccess = (cinemaList) => {
  return {
    type: FETCHCINEMALISTSUCCESS,
    payload: cinemaList,
  };
};
const fetchCinemaListFailed = (err) => {
  return {
    type: FETCHCINEMALISTFAILED,
    payload: err,
  };
};

// FETCH CINEMA BRANCH
export const fetchCinemaBranch = (maHeThongRap) => {
  return (dispatch) => {
    Axios.get(
      `https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`
    )
      .then((res) => {
        dispatch(fetchCinemaBranchSuccess(res.data, maHeThongRap));
        // console.log(res.data);
        // console.log(maHeThongRap);
      })
      .catch((err) => {
        dispatch(fetchCinemaBranchFailed);
        console.log(err);
      });
  };
};

const fetchCinemaBranchSuccess = (cinemaList, maHeThongRap) => {
  return {
    type: FETCHCINEMABRANCHSUCCESS,
    payload: cinemaList,
    id: maHeThongRap,
  };
};
const fetchCinemaBranchFailed = (err) => {
  return {
    type: FETCHCINEMABRANCHFAILED,
    payload: err,
  };
};
