let initialState = {};

const ticketReducer = (state = initialState, action) => {
  let { type, payload } = action;
  switch (type) {
    default:
      return state;
  }
};
export default ticketReducer;
