import {
  FETCHCINEMABRANCHSUCCESS,
  FETCHCINEMALISTSUCCESS,
} from "../constants/cinema.constant";

let initialState = {
  cinemaList: [],
  cinemaListBranch: {
    BHDStar: {},
    CGV: {},
    CineStar: {},
    Galaxy: {},
    LotteCinima: {},
    MegaGS: {},
  },
};

const cinemaReducer = (state = initialState, action) => {
  let { type, payload, id } = action;
  switch (type) {
    case FETCHCINEMALISTSUCCESS: {
      return { ...state, cinemaList: payload };
    }
    case FETCHCINEMABRANCHSUCCESS: {
      let newList = { ...state.cinemaListBranch };
      for (let cinemaItem in newList) {
        if (cinemaItem === id) {
          newList[cinemaItem] = payload;
        }
      }
      return { ...state, cinemaListBranch: newList };
    }
    default:
      return state;
  }
};
export default cinemaReducer;
